package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    long countByUserId(@NotNull String userId);

    @Nullable
    Project findByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findByUserIdAndName(@NotNull String userId, @NotNull String name);

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    void deleteAllByUserId(@NotNull String userId);

}
