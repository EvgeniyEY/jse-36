package ru.ermolaev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.dto.UserDTO;

public interface IUserEndpoint {

    void updateUserPassword(@Nullable SessionDTO sessionDTO, @Nullable String newPassword) throws Exception;

    void updateUserFirstName(@Nullable SessionDTO sessionDTO, @Nullable String newFirstName) throws Exception;

    void updateUserMiddleName(@Nullable SessionDTO sessionDTO, @Nullable String newMiddleName) throws Exception;

    void updateUserLastName(@Nullable SessionDTO sessionDTO, @Nullable String newLastName) throws Exception;

    void updateUserEmail(@Nullable SessionDTO sessionDTO, @Nullable String newEmail) throws Exception;

    @Nullable
    UserDTO findUserProfile(@Nullable SessionDTO sessionDTO) throws Exception;

}
