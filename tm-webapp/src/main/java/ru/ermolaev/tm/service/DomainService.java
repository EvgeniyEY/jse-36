package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.ermolaev.tm.api.service.IDomainService;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.Domain;

@Service
public class DomainService implements IDomainService {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IUserService userService;

    public DomainService(
            @NotNull final ITaskService taskService,
            @NotNull final IProjectService projectService,
            @NotNull final IUserService userService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
//        taskService.load(domain.getTaskDTOS());
//        projectService.load(domain.getProjectDTOS());
//        userService.load(domain.getUserDTOS());
    }

    @Override
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setTaskDTOS(taskService.findAll());
        domain.setProjectDTOS(projectService.findAll());
        domain.setUserDTOS(userService.findAll());
    }

}
